getResult = () => {
    let f = document.forms[0];
    let formdata = new FormData(f);

    let result = 0;
    let questionsCount = 0;
    for (let e of formdata.entries()) {
        questionsCount++;
        result += Number(e[1]);
    }

    let depression = (result / (questionsCount * 3) * 100).toFixed(2)
    
    alert(depression > 0 ? `У вас депрессия с вероятностью ${depression}%` : 'У вас нет депрессии');
}