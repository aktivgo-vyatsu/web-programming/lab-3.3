let unary_op = ['√', 'sin', 'cos', 'tan']

let calculate = (operation) => {
    let f = document.forms[0];
    let formdata = new FormData(f);

    let l_op = formdata.get('left-operand');
    let r_op = formdata.get('right-operand');

    try {
        validateFormInput(l_op, r_op, operation)
        l_op = Number(l_op)
        r_op = Number(r_op)
        validateNumberInput(l_op, r_op, operation)
    } catch (err) {
        alert(err)
        return
    }

    let result = 0;

    switch (operation) {
        case '+':
            result = l_op + r_op;
            break
        case '-':
            result = l_op - r_op;
            break
        case '*':
            result = l_op * r_op;
            break
        case '/':
            result = l_op / r_op;
            break
        case '√':
            result = Math.sqrt(l_op)
            break
        case '^':
            result = Math.pow(l_op, r_op)
            break
        case 'sin':
            result = Math.sin(l_op)
            break
        case 'cox':
            result = Math.cos(l_op)
            break
        case 'tan':
            result = Math.tan(l_op)
            break
    }

    document.getElementById('result').value = result.toString()
}

let validateFormInput = (l_op, r_op, operation) => {
    if (l_op === '') {
        throw 'Left operand is null'
    }

    if (r_op === '' && !unary_op.includes(operation)) {
        throw 'Right operand is null'
    }
}

let validateNumberInput = (l_op, r_op, operation) => {
    if (isNaN(l_op)) {
        throw 'Left operand not a number'
    }

    if (isNaN(r_op) && !unary_op.includes(operation)) {
        throw 'Right operand not a number'
    }
}

let clearScreen = () => {
    document.getElementById('left-operand').value = ''
    document.getElementById('right-operand').value = ''
    document.getElementById('result').value = ''
}